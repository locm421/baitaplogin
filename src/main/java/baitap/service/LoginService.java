package baitap.service;

import java.util.List;

import baitap.entity.NguoiDung;
import baitap.repository.NguoiDungRepository;
import jakarta.servlet.http.HttpSession;



public class LoginService {
	private NguoiDungRepository nguoiDungRepository = new NguoiDungRepository();
	public boolean checkLogin(String email, String password) {
		List<NguoiDung> listNguoiDung = new NguoiDungRepository().findByEmailAndPassword(email, password);
		return listNguoiDung.size() > 0;
	}
}
