package baitap.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConfig {
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3307/bt","root" ,"mypassword");
		} catch (ClassNotFoundException  |SQLException e ) {
			System.out.println("Lỗi kết nối CSDL" + e.getLocalizedMessage());
		}
		return connection;
	}
}
