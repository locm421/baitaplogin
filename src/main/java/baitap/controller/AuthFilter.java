package baitap.controller;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = "/login")
public class AuthFilter implements Filter{
	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)arg0;
		HttpServletResponse resp = (HttpServletResponse)arg1;
		if(!req.getServletPath().startsWith("/login"))
		{
			if(req.getSession().getAttribute("USSER_LOGIN")!= null) {
				arg2.doFilter(arg0, arg1);
			}
			else
			{
				resp.sendRedirect(req.getContentType()+"/login");
			}
		}
		else
		{
			arg2.doFilter(arg0, arg1);
		}
		
	}
}
