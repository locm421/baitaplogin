package baitap.controller;

import java.io.IOException;

import baitap.entity.NguoiDung;
import baitap.service.LoginService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name ="loginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet{
	LoginService loginService = new LoginService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("login.jsp").forward(req, resp);;
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String email = req.getParameter("email");
		String matkhau = req.getParameter("password");
		NguoiDung nguoiDung = new NguoiDung();
		nguoiDung.setEmail(email);
		nguoiDung.setMatkhau(matkhau);
		boolean isSuccess = loginService.checkLogin(email, matkhau);
		if(isSuccess)
		{
			HttpSession session = req.getSession();
			session.setAttribute("LOGIN_USER", nguoiDung);
			resp.sendRedirect(req.getContextPath() + "/home");
			
		}else {
			req.setAttribute("message", "Sai tên đăng nhập hoặc mật khẩu");
			req.getRequestDispatcher("login.jsp").forward(req, resp);
		}
		
	}
}
