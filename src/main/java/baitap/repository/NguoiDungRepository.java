package baitap.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import baitap.config.MysqlConfig;
import baitap.entity.NguoiDung;

public class NguoiDungRepository {
	public List<NguoiDung> findByEmailAndPassword(String email, String matkhau)
	{
		Connection connection = MysqlConfig.getConnection();
		String query = "SELECT * FROM NguoiDung nd WHERE nd.email = ? AND nd.matkhau = ?";
		
		List<NguoiDung> listNguoiDung = new ArrayList<NguoiDung>();
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, email);
			statement.setString(2, matkhau);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				NguoiDung nguoiDung = new NguoiDung();
				nguoiDung.setId(resultSet.getInt("id"));
				nguoiDung.setFullname(resultSet.getString("fullname"));
				nguoiDung.setEmail(resultSet.getString("email"));
				listNguoiDung.add(nguoiDung);
			}
			
		} catch (SQLException e) {
			System.out.println("Lỗi thực thi câu query" + e.getLocalizedMessage());
		}finally {
			if(connection != null)
			{
				try {
					connection.close();
				} catch (Exception e) {
					System.out.println("Lỗi đóng kết nối" + e.getLocalizedMessage());
				}
			}
			return listNguoiDung;
		
	}
	}
}
